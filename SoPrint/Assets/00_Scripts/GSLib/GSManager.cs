﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks;
using GameSparks.Core;

public class GSManager : MonoBehaviour 
{

	[SerializeField] private string msLogin	;
	[SerializeField] private string msPassword;


	void Start()
	{
		Debug.Log ("Start GS Connection");
		GSConnection ();
	}


	void GSConnection()
	{
		new GameSparks.Api.Requests.AuthenticationRequest()
			.SetUserName(msLogin)
			.SetPassword(msPassword)
			.Send((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("GS : Client Connected...");
				}
				else
				{
					Debug.LogError("GS : Connection Error");
				}
			});
	}

}
