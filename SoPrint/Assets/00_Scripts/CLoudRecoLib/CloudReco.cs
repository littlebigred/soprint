﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CloudReco : MonoBehaviour, ICloudRecoEventHandler
{

	private CloudRecoBehaviour mCloudRecoBehaviour;
	private bool mIsScanning = false;
	private string mTargetMetadata = "";

	// Use this for initialization
	void Start () 
	{
		// register this event handler at the cloud reco behaviour
		mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();

		if (mCloudRecoBehaviour)
		{
			mCloudRecoBehaviour.RegisterEventHandler(this);
		}
	}


	#region ICloudRecoEventHandler implementation
	public void OnInitialized ()
	{
		Debug.Log ("Vuforia CloudReco Init...");
//		throw new System.NotImplementedException ();
	}
	public void OnInitError (TargetFinder.InitState initError)
	{
		Debug.Log ("Vuforia CloudReco init error " + initError.ToString());
//		throw new System.NotImplementedException ();
	}
	public void OnUpdateError (TargetFinder.UpdateState updateError)
	{
		Debug.Log ("Vuforia CloudReco update error " + updateError.ToString());
//		throw new System.NotImplementedException ();
	}
	public void OnStateChanged (bool scanning)
	{
		mIsScanning = scanning;
		if (scanning)
		{
			Debug.Log ("Scanning ..." + mIsScanning);
			// clear all known trackables
//			ImageTracker tracker = TrackerManager.Instance.GetTracker<ImageTracker>();
//			tracker.TargetFinder.ClearTrackables(false);
		}

//		throw new System.NotImplementedException ();
	}
	public void OnNewSearchResult (TargetFinder.TargetSearchResult targetSearchResult)
	{
		// do something with the target metadata
		mTargetMetadata = targetSearchResult.MetaData;
		Debug.Log ("Vuforia CloudReco meta " + mTargetMetadata);
		// stop the target finder (i.e. stop scanning the cloud)
		mCloudRecoBehaviour.CloudRecoEnabled = false;
//		throw new System.NotImplementedException ();
	}


	void OnGUI() {
		// Display current 'scanning' status
		GUI.Box (new Rect(100,100,200,50), mIsScanning ? "Scanning" : "Not scanning");
		// Display metadata of latest detected cloud-target
		GUI.Box (new Rect(100,200,200,50), "Metadata: " + mTargetMetadata);
		// If not scanning, show button
		// so that user can restart cloud scanning
		if (!mIsScanning) {
			if (GUI.Button(new Rect(100,300,200,50), "Restart Scanning")) {
				// Restart TargetFinder
				mCloudRecoBehaviour.CloudRecoEnabled = true;
			}
		}
	}
	#endregion
}
